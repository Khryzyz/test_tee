<?php

	use App\Globals\Parametricas;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateRoomsTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			Schema::create('rooms', function (Blueprint $table) {
				$table->increments('id');
				$table->string('number');
				$table->unsignedInteger('type_room_id')->default(Parametricas::TYPE_ROOM_REGULAR);
				$table->timestamps();
				$table->softDeletes();

				//Creacion de la llave foranea a la tabla type_room
				$table->foreign('type_room_id')
					  ->references('id')->on('type_rooms')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			Schema::dropIfExists('rooms');
		}
	}
