<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateReservationsTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			Schema::create('reservations', function (Blueprint $table) {
				$table->increments('id');
				$table->unsignedInteger('room_id');
				$table->date('checkin');
				$table->date('checkout');
				$table->enum('status', ['A', 'O'])->default('A');
				$table->timestamps();
				$table->softDeletes();

				//Creacion de la llave foranea a la tabla type_room
				$table->foreign('room_id')
					  ->references('id')->on('rooms')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			Schema::dropIfExists('reservations');
		}
	}
