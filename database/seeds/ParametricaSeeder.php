<?php

	use App\Globals\Parametricas;
	use App\Models\TypeRoom;
	use Illuminate\Database\Seeder;

	class ParametricaSeeder extends Seeder {

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {

			/**
			 * Tipos de cuarto
			 */

			TypeRoom::create([
				'id'     => Parametricas::TYPE_ROOM_PREMIUM,
				'nombre' => "Premium",
			]);

			TypeRoom::create([
				'id'     => Parametricas::TYPE_ROOM_SPECIAL,
				'nombre' => "Special",
			]);

			TypeRoom::create([
				'id'     => Parametricas::TYPE_ROOM_REGULAR,
				'nombre' => "Regular",
			]);

		}
	}
