<?php

	use App\Globals\Parametricas;
	use App\Models\Reservation;
	use App\Models\Room;
	use Illuminate\Database\Seeder;

	class InitialSeeder extends Seeder {

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {

			/**
			 * Rooms
			 */
			$room = Room::create(['number' => '101', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '102', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '103', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '104', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '105', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '106', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '107', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '108', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '109', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '110', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '201', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '202', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '203', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '204', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '205', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '206', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '207', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '208', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '209', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '210', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '301', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '302', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '303', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '304', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '305', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '306', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '307', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '308', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '309', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '310', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '401', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '402', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '403', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '404', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '405', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '406', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '407', 'type_room_id' => Parametricas::TYPE_ROOM_REGULAR,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '408', 'type_room_id' => Parametricas::TYPE_ROOM_SPECIAL,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '409', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);

			$room = Room::create(['number' => '410', 'type_room_id' => Parametricas::TYPE_ROOM_PREMIUM,]);
			$this->addReservations($room->id);


		}

		public function addReservations($id) {

			$faker = Faker\Factory::create();

			for ($i = 0; $i < rand(1, 10); $i++) {
				Reservation::create(
					[
						'room_id'  => $id,
						'status'   => $faker->randomElement($array = ['A', 'O',]),
						'checkin'  => $faker->date($format = 'Y-m-d', $max = 'now'),
						'checkout' => $faker->date($format = 'Y-m-d', $max = 'now'),
					]
				);
			}
		}

	}
