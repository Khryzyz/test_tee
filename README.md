Prueba de Backend para Tee S.A.S

Para la prueba se hizo y se entrega en el presente repositorio lo siguiente:

Migraciones para el modelo de datos de:
    - Room
    - Type Room
    - Reservation
    
Datos semilla:
    - Pisos con 10 habitaciones 
    - Reservas aleatorias entre 1 y 10 para cada habitacion
    - Tres tipos de habitacion, Regular, Special, Premium

Controladores tipo resource:
    - Type Room (Listar, detalle)
    - Room (Crear, Listar, Actualizar, Eliminar, Detalle)
    - Reservation (Crear, Listar, Actualizar, Eliminar, Detalle)

Validaciones de Request:
    - Room (Número y tipo de cuarto obligatorios)
    - Reservation (Checkin, checkout y status obligatorios)

Repositorios para los metodos de los controladores:
    - Type Room (Listar, detalle)
    - Room (Crear, Listar, Actualizar, Eliminar, Detalle)
    - Reservation (Crear, Listar, Actualizar, Eliminar, Detalle)

Modelos relacionales de gestion
    - Type Room
    - Room
    - Reservation

Creacion de datos clases globales parametricas, para consistencia de datos.

Creacion de metodos de formateo de respuesta para gestion de la API.

Validacion de rutas y metodos

+--------+-----------+------------------------------------+---------------------+----------------------------------------------------+------------+
| Domain | Method    | URI                                | Name                | Action                                             | Middleware |
+--------+-----------+------------------------------------+---------------------+----------------------------------------------------+------------+
|        | GET|HEAD  | /                                  |                     | Closure                                            | web        |
|        | POST      | api/reservation                    | reservation.store   | App\Http\Controllers\ReservationController@store   | api        |
|        | GET|HEAD  | api/reservation                    | reservation.index   | App\Http\Controllers\ReservationController@index   | api        |
|        | GET|HEAD  | api/reservation/create             | reservation.create  | App\Http\Controllers\ReservationController@create  | api        |
|        | DELETE    | api/reservation/{reservation}      | reservation.destroy | App\Http\Controllers\ReservationController@destroy | api        |
|        | PUT|PATCH | api/reservation/{reservation}      | reservation.update  | App\Http\Controllers\ReservationController@update  | api        |
|        | GET|HEAD  | api/reservation/{reservation}      | reservation.show    | App\Http\Controllers\ReservationController@show    | api        |
|        | GET|HEAD  | api/reservation/{reservation}/edit | reservation.edit    | App\Http\Controllers\ReservationController@edit    | api        |
|        | GET|HEAD  | api/room                           | room.index          | App\Http\Controllers\RoomController@index          | api        |
|        | POST      | api/room                           | room.store          | App\Http\Controllers\RoomController@store          | api        |
|        | GET|HEAD  | api/room/create                    | room.create         | App\Http\Controllers\RoomController@create         | api        |
|        | DELETE    | api/room/{room}                    | room.destroy        | App\Http\Controllers\RoomController@destroy        | api        |
|        | PUT|PATCH | api/room/{room}                    | room.update         | App\Http\Controllers\RoomController@update         | api        |
|        | GET|HEAD  | api/room/{room}                    | room.show           | App\Http\Controllers\RoomController@show           | api        |
|        | GET|HEAD  | api/room/{room}/edit               | room.edit           | App\Http\Controllers\RoomController@edit           | api        |
|        | GET|HEAD  | api/typeRoom                       | typeRoom.index      | App\Http\Controllers\TypeRoomController@index      | api        |
|        | GET|HEAD  | api/typeRoom/{typeRoom}            | typeRoom.show       | App\Http\Controllers\TypeRoomController@show       | api        |
+--------+-----------+------------------------------------+---------------------+----------------------------------------------------+------------+


    

