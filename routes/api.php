<?php
	Route::resource('typeRoom', 'TypeRoomController', ['only' => ['show', 'index']]);

	Route::resource('room', 'RoomController');

	Route::resource('reservation', 'ReservationController');