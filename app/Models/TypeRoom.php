<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class TypeRoom extends Model {

		public $timestamps = false;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'id',
			'nombre',
		];

		/**
		 * Metodo que relaciona con Room
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\HasMany
		 */
		public function room() {

			return $this->hasMany(Room::class);
		}

	}
