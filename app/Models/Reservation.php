<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Reservation extends Model {


		use SoftDeletes;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'room_id',
			'checkin',
			'checkout',
			'status',
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'deleted_at',
		];

		/**
		 *  The attributes that are considered by softDelete
		 *
		 * @var array
		 */
		protected $dates = [
			'created_at',
			'updated_at',
			'deleted_at',
		];

		/**
		 * Relaciones ***********************************************************************************************
		 */

		/**
		 * Metodo que relaciona con Room
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function room() {

			return $this->belongsTo(room::class);
		}

	}
