<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Room extends Model {

		use SoftDeletes;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'number',
			'type_room_id',
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'deleted_at',
		];

		/**
		 *  The attributes that are considered by softDelete
		 *
		 * @var array
		 */
		protected $dates = [
			'created_at',
			'updated_at',
			'deleted_at',
		];

		/**
		 * Relaciones ***********************************************************************************************
		 */

		/**
		 * Metodo que relaciona con TypeRoom
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function type_room() {

			return $this->belongsTo(TypeRoom::class);
		}

		/**
		 * Metodo que relaciona con Reservation
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\HasMany
		 */
		public function reservation() {

			return $this->hasMany(Reservation::class);
		}
	}
