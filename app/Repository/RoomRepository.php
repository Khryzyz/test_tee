<?php

	namespace App\Repository;

	use App\Models\Room;
	use Illuminate\Database\Eloquent\ModelNotFoundException;

	class RoomRepository {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return Room::with('type_room', 'reservation')->get();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return Room::where('id', $id)->with('type_room', 'reservation')->get();
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			return Room::create($request->all());
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			try {
				return Room::findorfail($id)->update($request->all());
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			try {
				return Room::findorfail($id)->delete();
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}
	}
