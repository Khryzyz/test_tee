<?php

	namespace App\Repository;

	use App\Models\TypeRoom;

	class TypeRoomRepository {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return TypeRoom::all();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return TypeRoom::findorfail($id);
		}

	}
