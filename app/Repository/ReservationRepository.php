<?php

	namespace App\Repository;

	use App\Models\Reservation;
	use Illuminate\Database\Eloquent\ModelNotFoundException;

	class ReservationRepository {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return Reservation::with('room')->get();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return Reservation::findorfail($id);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			return Reservation::create($request->all());
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			try {
				return Reservation::findorfail($id)->update($request->all());
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			try {
				return Reservation::findorfail($id)->delete();
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}
	}
