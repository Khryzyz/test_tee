<?php

	namespace App\Http\Controllers;

	use App\Globals\KeysResponse;
	use App\Globals\MethodsHttp;
	use App\Globals\Utils;
	use App\Http\Requests\RoomRequest;
	use App\Repository\RoomRepository;
	use Illuminate\Support\Facades\DB;

	class RoomController extends Controller {

		protected $repository;

		/**
		 * TareaController constructor.
		 *
		 * @param $repository
		 */
		public function __construct(RoomRepository $repository) {

			$this->repository = $repository;

		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			try {
				$data = $this->repository->index();

				return Utils::responseTransaccion(
					$data,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$data = $this->repository->show($id);

				return Utils::responseTransaccion(
					$data,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \App\Http\Requests\RoomRequest $request
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function store(RoomRequest $request) {

			try {
				DB::beginTransaction();
				$data = $this->repository->store($request);
				DB::commit();

				return Utils::responseTransaccion(
					$data,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_POST
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_POST
				);
			}
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param \App\Http\Requests\RoomRequest     $request
		 * @param                                    $id
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function update(RoomRequest $request, $id) {

			try {
				DB::beginTransaction();
				$data = $this->repository->update($request, $id);
				DB::commit();

				return Utils::responseTransaccion(
					$data,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_PUT
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_PUT
				);
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id) {

			try {
				DB::beginTransaction();
				$data = $this->repository->destroy($id);
				DB::commit();

				return Utils::responseTransaccion(
					$data,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_DELETE
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_DELETE
				);
			}
		}
	}
