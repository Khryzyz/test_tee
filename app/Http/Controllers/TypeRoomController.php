<?php

	namespace App\Http\Controllers;

	use App\Globals\KeysResponse;
	use App\Globals\MethodsHttp;
	use App\Globals\Utils;
	use App\Repository\TypeRoomRepository;

	class TypeRoomController extends Controller {

		protected $repository;

		/**
		 * TypeRoomController constructor.
		 *
		 * @param $repository
		 */
		public function __construct(TypeRoomRepository $repository) {

			$this->repository = $repository;

		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			try {
				$dataModel = $this->repository->index();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$dataModel = $this->repository->show($id);

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}


	}
