<?php

	namespace App\Http\Requests;

	use App\Globals\CodesResponse;
	use App\Globals\MethodsHttp;
	use Illuminate\Contracts\Validation\Validator;
	use Illuminate\Foundation\Http\FormRequest;
	use Illuminate\Http\Exceptions\HttpResponseException;

	class ReservationRequest extends FormRequest {

		/**
		 * Determine if the user is authorized to make this request.
		 *
		 * @return bool
		 */
		public function authorize() {

			return true;
		}

		/**
		 * Funcion que maneja las reglas de validacion
		 *
		 * @return array
		 */
		public function rules() {

			switch ($this->method()) {
				case MethodsHttp::METHOD_GET:
				case MethodsHttp::METHOD_DELETE:
					{
						return [];
					}
				case MethodsHttp::METHOD_POST:
					{
						return [
							'room_id'  => 'required',
							'checkin'  => 'required',
							'checkout' => 'required',
						];
					}
				case MethodsHttp::METHOD_PUT:
					{
						return [
							'room_id'  => 'required',
							'checkin'  => 'required',
							'checkout' => 'required',
						];
					}
				default:
					return null;
			}
		}

		/**
		 * Funcion que maneja los mensajes de la validacion
		 *
		 * @return array
		 */
		public function messages() {

			return [
				'required' => 'El campo :attribute es requerido.',
			];
		}

		/**
		 * Funcion que maneja los nombres alternativos de la validacion
		 *
		 * @return array
		 */
		public function attributes() {

			return [
				'checkin'  => 'Fecha de llegada',
				'checkout' => 'Fecha de salida',
				'status'   => 'Estado de la habitación',
			];
		}

		/**
		 * Metodo que evita la redireccion en caso de fallo
		 *
		 * @param \Illuminate\Contracts\Validation\Validator $validator
		 */
		protected function failedValidation(Validator $validator) {

			throw new HttpResponseException(
				response()->json(
					$validator->errors(),
					CodesResponse::CODE_FORM_INVALIDATE
				)
			);
		}
	}
