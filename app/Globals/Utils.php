<?php

	namespace App\Globals;

	class Utils {

		public static function responseTransaccion($dataModel, $status, $method, $text = "") {

			switch ($method) {
				case MethodsHttp::METHOD_GET:
					$messageSuccess  = MessageResponse::MESSAGE_QUERY_SUCCESS.", ".$text;
					$messageError    = MessageResponse::MESSAGE_QUERY_ERROR.", ".$text;
					$messageNotFound = MessageResponse::MESSAGE_QUERY_EMPTY.", ".$text;
					break;
				case MethodsHttp::METHOD_POST:
					$messageSuccess  = MessageResponse::MESSAGE_CREATE_SUCCESS.", ".$text;
					$messageError    = MessageResponse::MESSAGE_CREATE_ERROR.", ".$text;
					$messageNotFound = $messageError;
					break;
				case MethodsHttp::METHOD_PUT:
					$messageSuccess  = MessageResponse::MESSAGE_UPDATE_SUCCESS.", ".$text;
					$messageError    = MessageResponse::MESSAGE_UPDATE_ERROR.", ".$text;
					$messageNotFound = $messageError;
					break;
				case MethodsHttp::METHOD_DELETE:
					$messageSuccess  = MessageResponse::MESSAGE_DELETE_SUCCESS.", ".$text;
					$messageError    = MessageResponse::MESSAGE_DELETE_ERROR.", ".$text;
					$messageNotFound = $messageError;
			}
			if ($status == KeysResponse::STATUS_SUCCESS) {
				if ($dataModel != null) {
					return response()->json(
						[
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
							KeysResponse::KEY_MESSAGE => $messageSuccess,
							KeysResponse::KEY_DATA    => $dataModel,
						],
						CodesResponse::CODE_OK
					);
				}
				else {
					return response()->json(
						[
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_NOT_FOUND,
							KeysResponse::KEY_MESSAGE => $messageNotFound,
							KeysResponse::KEY_DATA    => null,
						],
						CodesResponse::CODE_NOT_FOUND
					);
				}
			}
			else {
				return response()->json(
					[
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => $messageError,
						KeysResponse::KEY_DATA    => null,
					],
					CodesResponse::CODE_INTERNAL_SERVER
				);
			}
		}
	}
