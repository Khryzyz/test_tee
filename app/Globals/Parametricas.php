<?php

	namespace App\Globals;

	class Parametricas {

		/**
		 * @global int tipo de cuarto
		 */
		const TYPE_ROOM_PREMIUM = 1;

		/**
		 * @global int tipo de cuarto
		 */
		const TYPE_ROOM_SPECIAL = 2;

		/**
		 * @global int tipo de cuarto
		 */
		const TYPE_ROOM_REGULAR = 3;

	}
